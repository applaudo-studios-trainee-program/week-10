exports.createPages = async ({ actions: { createPage }, graphql }) => {
  const {
    data: { dataJson: homeData },
  } = await graphql(`
    {
      dataJson {
        personalInfo {
          title
          description
          profileImage {
            childImageSharp {
              fluid(quality: 100) {
                base64
                aspectRatio
                src
                srcSet
                sizes
              }
            }
          }
        }
      }
    }
  `);

  const {
    data: {
      allProjectsDataJson: { edges },
    },
  } = await graphql(`
    {
      allProjectsDataJson {
        edges {
          node {
            title
            stack
            slug
            desktopImages {
              childImageSharp {
                fluid(maxWidth: 1920, quality: 100) {
                  base64
                  aspectRatio
                  src
                  srcSet
                  sizes
                }
              }
            }
          }
        }
      }
    }
  `);

  const projectDataList = edges.map(projectData => ({ ...projectData.node }));

  const projectSlugList = await graphql(`
    {
      allProjectsDataJson {
        edges {
          node {
            slug
          }
        }
      }
    }
  `);

  if (projectSlugList.error) {
    console.log('Something went wrong');
  }

  createPage({
    component: require.resolve(`${__dirname}/src/templates/home.tsx`),
    context: { ...homeData },
    path: '/',
  });

  createPage({
    component: require.resolve(`${__dirname}/src/templates/projects.tsx`),
    context: { projectDataList },
    path: '/projects',
  });

  projectSlugList.data.allProjectsDataJson.edges.forEach(edge => {
    const project = edge.node;

    createPage({
      component: require.resolve(`${__dirname}/src/templates/project.tsx`),
      path: `/project/${project.slug}`,
      context: { slug: project.slug },
    });
  });
};

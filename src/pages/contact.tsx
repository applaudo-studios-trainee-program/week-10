import { Link } from 'gatsby';
import React from 'react';
import { Helmet } from 'react-helmet';
import GitHub from '../../static/icons/GitHub';
import LinkedIn from '../../static/icons/LinkedIn';
import Twitter from '../../static/icons/Twitter';
import Button from '../components/Button';
import Column from '../components/Layouts/Alignment/Column';
import Row from '../components/Layouts/Alignment/Row';

import MainLayout from '../components/Layouts/MainLayout';
import Wrapper from '../components/Layouts/Wrapper';

const Contact = () => {
  return (
    <>
      <Helmet title="bMend_ | Contact" />

      <MainLayout>
        <Wrapper>
          <Column gap={3} alignItems="center">
            <Column gap={1.5} alignItems="center">
              <h1>Contact Me</h1>
              <em>You can contact me and let me know your plans</em>

              <h3>Sometimes, sending a Tweet may not feel appropriate</h3>

              <p style={{ maxWidth: '50rem', textAlign: 'center' }}>
                You can use the options below to contact me. The 95% of the
                time, you can expect a response within the same day of sending a
                message. If you do not receive a reply within 3 days, feel free
                to send me another message.
              </p>
            </Column>

            <Column gap={1} alignItems="center">
              <h3>Social</h3>

              <Row flex gap={3}>
                <Column gap={0.5}>
                  <em>Twitter</em>

                  <Row justifyContent="center">
                    <Link
                      to="https://twitter.com/beMendoza_"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <Twitter />
                    </Link>
                  </Row>
                </Column>

                <Column gap={0.5} justifyContent="center">
                  <em>LinkedIn</em>

                  <Row justifyContent="center">
                    <Link
                      to="https://www.linkedin.com/in/bemendoza/"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <LinkedIn />
                    </Link>
                  </Row>
                </Column>

                <Column gap={0.5} justifyContent="center">
                  <em>GitHub</em>

                  <Row justifyContent="center">
                    <Link
                      to="https://github.com/b-mendoza"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <GitHub />
                    </Link>
                  </Row>
                </Column>
              </Row>
            </Column>

            <Column>
              <a
                href="mailto:bmendoza.dev@hotmail.com"
                style={{ maxWidth: 'max-content' }}
              >
                <Button>
                  <strong>🔗 Send me an Email</strong>
                </Button>
              </a>
            </Column>
          </Column>
        </Wrapper>
      </MainLayout>
    </>
  );
};

export default Contact;

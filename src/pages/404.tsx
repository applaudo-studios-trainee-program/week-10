import React from 'react';
import { Helmet } from 'react-helmet';
import styled from '@emotion/styled';

import MainLayout from '../components/Layouts/MainLayout';
import Column from '../components/Layouts/Alignment/Column';
import Wrapper from '../components/Layouts/Wrapper';
import { Link } from 'gatsby';

const ErrorImage = styled.img`
  border-radius: var(--border-radius);

  height: auto;
  width: 15em;
`;

const NotFound = () => (
  <>
    <Helmet title="bMend_ | Page not Found" />

    <Wrapper>
      <Column
        gap={1.5}
        alignItems="center"
        justifyContent="center"
        style={{ height: 'calc(100vh - 6rem)' }}
      >
        <h2>
          Oh no!... Looks like this little guy <em>fell asleep</em>
        </h2>

        <ErrorImage src="https://media.giphy.com/media/ngBu7pEudTTSU/giphy.gif" />

        <em>While he gets ready to work you can return to</em>

        <h3>
          <Link to="/">😄 Home</Link>
        </h3>
      </Column>
    </Wrapper>
  </>
);

export default NotFound;

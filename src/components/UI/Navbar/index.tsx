import React, { useState } from 'react';
import { Link } from 'gatsby';
import styled from '@emotion/styled';
import tw from 'twin.macro';

import BurgerMenu from '../../../../static/icons/BurgerMenu';

import styles from './Navbar.module.scss';

const SubTitle = styled.h3`
  ${tw`text-3xl font-black`}
`;

const Navbar = () => {
  const [showMenu, setShowMenu] = useState(false);

  const handleIconClick = () => setShowMenu(showMenu => !showMenu);

  const handleAnchorClick = () => {
    setShowMenu(showMenu => showMenu && !showMenu);
  };

  return (
    <header className={styles.container}>
      <nav className={styles.menu}>
        <Link className={styles.title} to="/" onClick={handleAnchorClick}>
          <SubTitle>bMend_</SubTitle>
        </Link>

        <ul
          className={
            showMenu
              ? `${styles.menuList} ${styles.show}`
              : `${styles.menuList}`
          }
        >
          <li className={styles['menuItem']}>
            <Link
              className={styles['menuAnchor']}
              to="/"
              onClick={handleAnchorClick}
            >
              Home
            </Link>
          </li>
          <li className={styles['menuItem']}>
            <Link
              className={styles['menuAnchor']}
              to="/projects"
              onClick={handleAnchorClick}
            >
              Projects
            </Link>
          </li>
          <li className={styles['menuItem']}>
            <Link
              className={styles['menuAnchor']}
              to="/contact"
              onClick={handleAnchorClick}
            >
              Contact Me
            </Link>
          </li>
        </ul>

        <BurgerMenu className={styles.menuIcon} onClick={handleIconClick} />
      </nav>
    </header>
  );
};

export default Navbar;

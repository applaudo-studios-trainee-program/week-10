import { ComponentProps } from 'react';
import styled from '@emotion/styled';

import Row from '../Layouts/Alignment/Row';

type Props = ComponentProps<'button'>;

const StyledButton = styled.button`
  border-radius: var(--border-radius);
  border: 0.1rem solid var(--primary-dark-color);

  color: rgba(31, 31, 35, 0.541);

  max-width: max-content;

  font-family: inherit;

  &:hover {
    color: rgb(31, 31, 35);
  }
`;

const Button = ({ children, ...defaultProps }: Props) => (
  <StyledButton {...defaultProps}>
    <Row gap={1} alignItems="center">
      {children}
    </Row>
  </StyledButton>
);

export default Button;

import React, { ComponentProps } from 'react';
import styled from '@emotion/styled';

type BaseProps = {
  tagTitle: string;
  tagId?: string;
  tagName?: string;
  tagValue?: string;
};

type Props = BaseProps &
  Omit<
    ComponentProps<'input'>,
    'className' | 'id' | 'name' | 'value' | keyof BaseProps
  >;

const TagInput = styled.input`
  display: none;

  pointer-events: none;

  &:checked + label {
    background-color: #18181b;
  }
`;

const TagLabel = styled.label`
  background-color: rgba(24, 24, 27, 0.5);

  border-radius: var(--border-radius);

  color: var(--primary-white-color);

  cursor: pointer;

  display: inline-block;

  height: 100%;
  width: 100%;

  padding: 0.5rem 1.5rem;

  &:hover {
    background-color: #18181b;
  }
`;

const Tag = ({
  tagTitle,
  tagId,
  tagName,
  tagValue,
  ...defaultProps
}: Props) => (
  <div>
    <TagInput
      type="checkbox"
      name={tagName}
      id={tagId}
      value={tagValue}
      {...defaultProps}
    />
    <TagLabel htmlFor={tagId}>{tagTitle}</TagLabel>
  </div>
);

export default Tag;

import React from 'react';
import Img, { FluidObject } from 'gatsby-image';
import styled from '@emotion/styled';
import Column from '../Layouts/Alignment/Column';

type Props = {
  title: string;
  stack: string[];
  image: FluidObject;
};

const CardWrapper = styled.div`
  color: var(--primary-white-color);
`;

const CardImage = styled(Img)`
  border-top-left-radius: var(--border-radius);
  border-top-right-radius: var(--border-radius);

  height: auto;
  width: 100%;
`;

const CardData = styled.div`
  background-color: var(--secondary-dark-color);

  border-bottom-left-radius: var(--border-radius);
  border-bottom-right-radius: var(--border-radius);

  padding: 2rem;
`;

const Card = ({ title, stack, image }: Props) => {
  return (
    <CardWrapper>
      <Column>
        <CardImage fluid={image} />

        <CardData>
          <Column gap={0.5}>
            <h4>{title}</h4>

            <small>
              {stack.map((technology, index) =>
                index === 0 ? technology : `, ${technology}`
              )}
            </small>
          </Column>
        </CardData>
      </Column>
    </CardWrapper>
  );
};

export default Card;

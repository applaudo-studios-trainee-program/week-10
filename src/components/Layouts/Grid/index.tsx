import { memo } from 'react';
import styled from '@emotion/styled';

type LayoutProps = {
  gap?: number;
};

const Grid = styled.div<LayoutProps>`
  display: grid;

  ${({ gap }) => (gap ? `gap: ${gap}rem;` : null)}

  grid-auto-rows: min-content;
  grid-template-columns: 1fr;

  @media (min-width: 650px) {
    grid-template-columns: repeat(2, 1fr);
  }
  /* 
  @media (min-width: 992px) {
    grid-template-columns: repeat(3, 1fr);
  } */
`;

export default memo(Grid);

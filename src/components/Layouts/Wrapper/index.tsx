import styled from '@emotion/styled';
import { memo } from 'react';

const Wrapper = styled.section`
  padding: 2rem;

  margin: 0 auto;

  max-width: 120rem;

  @media (min-width: 340px) {
    padding: 3rem;
  }
`;

export default memo(Wrapper);

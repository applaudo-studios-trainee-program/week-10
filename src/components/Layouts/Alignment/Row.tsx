import { memo } from 'react';
import styled from '@emotion/styled';

type LayoutProps = {
  alignItems?: 'start' | 'center' | 'end';
  flex?: boolean;
  gap?: number;
  justifyContent?: 'start' | 'center' | 'end';
};

const Row = styled.div<LayoutProps>`
  display: flex;

  ${({ flex }) => (flex ? 'flex: 1;' : null)}
  flex-direction: row;
  flex-wrap: wrap;

  ${({ alignItems }) =>
    alignItems === 'center'
      ? `align-items: ${alignItems};`
      : `align-items: flex-${alignItems};`}

  ${({ justifyContent }) =>
    justifyContent === 'center'
      ? `justify-content: ${justifyContent};`
      : `justify-content: flex-${justifyContent};`}

  ${({ gap }) => (gap ? `gap: ${gap}rem;` : null)}
`;

export default memo(Row);

import React, { ReactNode } from 'react';

import Navbar from '../../UI/Navbar';

type Props = {
  children: ReactNode;
};

const MainLayout = ({ children }: Props) => (
  <>
    <Navbar />

    <main>{children}</main>
  </>
);

export default MainLayout;

import { memo } from 'react';
import styled from '@emotion/styled';

const MasonryLayout = styled.div`
  column-count: 1;

  @media (min-width: 576px) {
    column-count: 2;
  }
`;

export default memo(MasonryLayout);

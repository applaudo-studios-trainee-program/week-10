import React, { ChangeEvent, useRef, useState } from 'react';
import { Helmet } from 'react-helmet';
import { Link } from 'gatsby';

import { Project } from '../interfaces';
import Card from '../components/Card';
import Column from '../components/Layouts/Alignment/Column';
import Grid from '../components/Layouts/Grid';
import MainLayout from '../components/Layouts/MainLayout';
import Row from '../components/Layouts/Alignment/Row';
import Tag from '../components/Tag';
import Wrapper from '../components/Layouts/Wrapper';

type Props = {
  pageContext: {
    projectDataList: Pick<
      Project,
      'title' | 'stack' | 'slug' | 'desktopImages'
    >[];
  };
};

const Projects = ({ pageContext }: Props) => {
  const { projectDataList } = pageContext;

  const tagsRef = useRef([
    ...new Set(
      projectDataList
        .map(project => project.stack)
        .reduce((acc, curVal) => acc.concat(curVal), [])
    ),
  ]);

  const [filteredTags, setFilteredTags] = useState<string[]>([]);

  const handleTagChange = (event: ChangeEvent<HTMLInputElement>) => {
    const tagValue = event.target.value;

    event.target.checked
      ? setFilteredTags(filterTags => [...filterTags, tagValue])
      : setFilteredTags(filterTags =>
          filterTags.filter(tagName => tagName !== tagValue)
        );
  };

  const filteredProjects = filteredTags.length
    ? projectDataList.filter(project =>
        project.stack.find(technology => filteredTags.includes(technology))
      )
    : projectDataList;

  return (
    <>
      <Helmet title="bMend_ | Projects" />

      <MainLayout>
        <Wrapper>
          <Column gap={3}>
            <Column gap={0.5}>
              <h1>My Projects</h1>
              <em>
                These are the different projects that I had the opportunity to
                be involved
              </em>
            </Column>

            <Row flex gap={2}>
              {tagsRef.current.map((tagName: any) => (
                <Tag
                  key={tagName}
                  tagTitle={tagName}
                  tagValue={tagName}
                  tagId={tagName}
                  onChange={handleTagChange}
                />
              ))}
            </Row>

            <hr />

            <Grid gap={3}>
              {filteredProjects.map(project => (
                <Link to={`/project/${project.slug}`} key={project.slug}>
                  <Card
                    image={project.desktopImages[0].childImageSharp.fluid}
                    {...project}
                  />
                </Link>
              ))}
            </Grid>
          </Column>
        </Wrapper>
      </MainLayout>
    </>
  );
};

export default Projects;

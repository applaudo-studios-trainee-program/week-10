import React from 'react';
import { graphql, Link } from 'gatsby';
import { Helmet } from 'react-helmet';
import Img from 'gatsby-image';
import styled from '@emotion/styled';

import { Project as ProjectData } from '../interfaces';
import Column from '../components/Layouts/Alignment/Column';
import Grid from '../components/Layouts/Grid';
import MainLayout from '../components/Layouts/MainLayout';
import MasonryImagesLayout from '../components/Layouts/MasonryLayout';
import Wrapper from '../components/Layouts/Wrapper';

export const query = graphql`
  query($slug: String!) {
    projectsDataJson(slug: { eq: $slug }) {
      title
      description
      stack
      layout
      deployLink
      desktopImages {
        childImageSharp {
          fluid(quality: 100) {
            base64
            aspectRatio
            src
            srcSet
            sizes
          }
        }
      }
      mobileImages {
        childImageSharp {
          fluid(quality: 100) {
            base64
            aspectRatio
            src
            srcSet
            sizes
          }
        }
      }
    }
  }
`;

const MasonryImage = styled(Img)`
  height: auto;
  width: 100%;

  border-radius: var(--border-radius);

  display: inline-block;

  &:not(:last-child) {
    margin-bottom: 2rem;
  }
`;

const StickyData = styled.div`
  @media (min-width: 768px) {
    position: sticky;

    top: calc(16rem);

    max-height: min-content;
  }
`;

const StyledLink = styled(Link)`
  background-color: var(--secondary-dark-color);

  border-radius: var(--border-radius);

  color: var(--primary-white-color);

  padding: 0.5rem;

  max-width: max-content;
`;

type Props = {
  data: {
    projectsDataJson: ProjectData;
  };
};

const Project = ({ data }: Props) => {
  const projectData = data.projectsDataJson;

  const {
    deployLink,
    description,
    desktopImages,
    mobileImages,
    stack,
    title,
  } = projectData;

  return (
    <>
      <Helmet title={`bMend_ | ${title}`} />

      <MainLayout>
        <Wrapper>
          <Grid gap={3}>
            <Column>
              <StickyData>
                <Column gap={2} justifyContent="center">
                  <Column gap={0.5}>
                    <h1>{title}</h1>
                    <em>{description}</em>
                  </Column>

                  <Column gap={0.5}>
                    <h4>Technologies that I use to build this</h4>

                    <span>
                      {stack.map((technology, index) =>
                        index === 0 ? technology : `, ${technology}`
                      )}
                    </span>
                  </Column>

                  <Column gap={0.5}>
                    <span>
                      You can see the full project {''}
                      <a
                        href={`${deployLink}`}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <strong>
                          <em>HERE</em>
                        </strong>
                      </a>
                    </span>
                  </Column>

                  <StyledLink to="/projects">
                    <em>&lsaquo; Return to Projects</em>
                  </StyledLink>
                </Column>
              </StickyData>
            </Column>

            <Column>
              <MasonryImagesLayout>
                {desktopImages.map((image, index) => (
                  <MasonryImage
                    fluid={image.childImageSharp.fluid}
                    key={index.toString()}
                  />
                ))}
                {mobileImages
                  ? mobileImages.map((image, index) => (
                      <MasonryImage
                        fluid={image.childImageSharp.fluid}
                        key={index.toString()}
                      />
                    ))
                  : null}
              </MasonryImagesLayout>
            </Column>
          </Grid>
        </Wrapper>
      </MainLayout>
    </>
  );
};

export default Project;

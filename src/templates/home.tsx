import React from 'react';
import { Helmet } from 'react-helmet';
import Img from 'gatsby-image';
import tw from 'twin.macro';
import styled from '@emotion/styled';

import { PersonalInfo } from '../interfaces';
import Column from '../components/Layouts/Alignment/Column';
import GitHub from '../../static/icons/GitHub';
import LinkedIn from '../../static/icons/LinkedIn';
import MainLayout from '../components/Layouts/MainLayout';
import Row from '../components/Layouts/Alignment/Row';
import Twitter from '../../static/icons/Twitter';
import Wrapper from '../components/Layouts/Wrapper';

const Grid = styled.div`
  display: grid;

  place-items: center;

  gap: 2rem;

  @media (min-width: 992px) {
    place-items: unset;

    grid-template-columns: repeat(2, 1fr);
  }

  @media (min-width: 1500px) {
    max-width: 150rem;
  }
`;

const TWTitle = styled.h1`
  ${tw`text-4xl font-black`}
`;

const SubTitle = styled.h3`
  ${tw`text-3xl font-black`}
`;

const Title = styled(TWTitle)`
  @media (min-width: 992px) {
    align-self: flex-end;

    justify-self: flex-end;
  }
`;

const TWFluidImage = styled(Img)`
  ${tw`rounded-full h-auto w-5/6 m-auto`}
`;

const FluidImage = styled(TWFluidImage)`
  @media (min-width: 992px) {
    grid-column: 2 / 3;
    grid-row: 1 / 3;

    justify-self: center;
  }
`;

const TWColumn = styled(Column)`
  ${tw`flex items-end`}
`;

const Description = styled.p`
  @media (min-width: 768px) {
    font-size: 1.1em;

    max-width: 55rem;

    place-self: flex-end;
  }

  @media (min-width: 992px) {
    text-align: right;
  }
`;

type Props = {
  pageContext: {
    personalInfo: PersonalInfo;
  };
};

const Home = ({ pageContext }: Props) => {
  const { title, description, profileImage } = pageContext.personalInfo;

  const imageMetaData = profileImage.childImageSharp.fluid;

  return (
    <>
      <Helmet title="bMend_ | Home" />

      <MainLayout>
        <Wrapper>
          <Column gap={3}>
            <Grid>
              <Title>{title}</Title>

              <FluidImage fluid={imageMetaData} />

              <Column gap={2}>
                <Description
                  dangerouslySetInnerHTML={{ __html: description }}
                />

                <TWColumn gap={1}>
                  <SubTitle className="text-4xl font-black">Social</SubTitle>

                  <Row flex gap={3} justifyContent="end">
                    <Column gap={0.5}>
                      <em>Twitter</em>

                      <Row justifyContent="center">
                        <a
                          href="https://twitter.com/beMendoza_"
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          <Twitter />
                        </a>
                      </Row>
                    </Column>

                    <Column gap={0.5} justifyContent="center">
                      <em>LinkedIn</em>

                      <Row justifyContent="center">
                        <a
                          href="https://www.linkedin.com/in/bemendoza/"
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          <LinkedIn />
                        </a>
                      </Row>
                    </Column>

                    <Column gap={0.5} justifyContent="center">
                      <em>GitHub</em>

                      <Row justifyContent="center">
                        <a
                          href="https://github.com/b-mendoza"
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          <GitHub />
                        </a>
                      </Row>
                    </Column>
                  </Row>
                </TWColumn>
              </Column>
            </Grid>

            <hr />

            <Column gap={3}>
              <SubTitle>Education</SubTitle>

              <Column gap={1}>
                <h3>High School and College</h3>

                <Row gap={2}>
                  <Column>
                    <em>2015 - 2017</em>
                    <strong>Bachelor of Graphic Design</strong>
                    <small>Colegio Don Bosco</small>
                  </Column>

                  <Column>
                    <em>2020 - Present</em>
                    <strong>Technical degree in Computer Science</strong>
                    <small>Universidad Don Bosco</small>
                  </Column>
                </Row>
              </Column>

              <Column gap={1}>
                <SubTitle>Certifications</SubTitle>

                <Row gap={2}>
                  <Column>
                    <em>2019</em>
                    <strong>Certification in HTML / CSS</strong>
                    <small>Platzi</small>
                  </Column>

                  <Column>
                    <em>2019</em>
                    <strong>Certification in HTML / CSS</strong>
                    <small>Udemy</small>
                  </Column>

                  <Column>
                    <em>2019</em>
                    <strong>Certification in SASS</strong>
                    <small>Udemy</small>
                  </Column>

                  <Column>
                    <em>2019</em>
                    <strong>Certification in JavaScript</strong>
                    <small>Udemy</small>
                  </Column>

                  <Column>
                    <em>2019</em>
                    <strong>Certification in React</strong>
                    <small>Udemy</small>
                  </Column>
                </Row>
              </Column>
            </Column>

            <Column gap={3}>
              <SubTitle>Experiencie</SubTitle>

              <Column gap={1}>
                <h3>Working Experience</h3>

                <Row gap={2}>
                  <Column>
                    <em>2020 - Present</em>
                    <strong>Trainee Program</strong>
                    <small>Applaudo Studios</small>
                  </Column>
                </Row>
              </Column>
            </Column>
          </Column>
        </Wrapper>
      </MainLayout>
    </>
  );
};

export default Home;

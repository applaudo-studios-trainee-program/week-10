export interface PersonalInfo {
  description: string;
  profileImage: FluidImage;
  title: string;
}

export interface FluidImage {
  childImageSharp: ChildImageSharp;
}

export interface ChildImageSharp {
  fluid: Fluid;
}

export interface Fluid {
  aspectRatio: number;
  base64: string;
  sizes: string;
  src: string;
  srcSet: string;
}

export interface Project {
  title: string;
  slug: string;
  description: string;
  stack: string[];
  layout: string;
  deployLink: string;
  desktopImages: FluidImage[];
  mobileImages?: FluidImage[];
}
